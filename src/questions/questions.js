export const questions = [
  [
    'Was ist ein OLAP-Würfel und wie unterscheidet er sich von einer klassischen relationalen Datenbankstruktur?',
    'Ein OLAP-Würfel ist eine multidimensionale Datenstruktur, die komplexe Daten in einer leicht verständlichen Form organisiert und analysiert. Im Gegensatz zu einer relationalen Datenbank, in der Daten in Tabellen gespeichert sind, verwendet ein OLAP-Würfel mehrere Dimensionen, ähnlich den Seiten eines Würfels, um Informationen darzustellen.',
  ],
  [
    'Was repräsentieren die Zellen eines OLAP-Würfels und wie werden aggregierte Werte bestimmt?',
    'Die Zellen des OLAP-Würfels repräsentieren aggregierte Werte, die durch das Zusammenspiel verschiedener Dimensionen bestimmt werden. Aggregierte Werte werden durch die Kombination von Hierarchieebenen in den Dimensionen erreicht, was tiefgehende Einblicke in komplexe Datenzusammenhänge ermöglicht.',
  ],
  [
    'Welche Funktion erfüllen Dimensionen in einem OLAP-Würfel? Können OLAP-Würfel mehr als drei Dimensionen haben?',
    'Dimensionen in einem OLAP-Würfel repräsentieren relevante Aspekte der zu organisierenden Daten. OLAP-Würfel können mehr als drei Dimensionen haben und jede Dimension kann in verschiedene Hierarchieebenen gegliedert sein.',
  ],
  [
    'Erklären Sie den Begriff Slicing im Kontext von OLAP-Würfeln und geben Sie ein Beispiel.',
    'Slicing bezieht sich auf die Herausnahme von Scheiben des Datenwürfels. Ein Beispiel dafür wäre die Anzeige der Verkaufszahlen für einen bestimmten Monat des Verkaufsjahres.',
  ],
  [
    'Welche Grundoperation ermöglicht das Drill-Down in einem OLAP-Würfel und wie wird es in der Datenanalyse eingesetzt?',
    'Die Operation Drill-Down ermöglicht das Hereinzoomen, bei dem aggregierte Informationen in detailliertere Werte aufgelöst werden. Es wird in der Datenanalyse verwendet, um beispielsweise von der Jahres- zur Monatsansicht zu wechseln und detaillierte Informationen zu erhalten.',
  ],
]
